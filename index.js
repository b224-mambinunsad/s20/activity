let number = Number(prompt("Enter a number: "));
console.log("The number you provided is: " + number);

for(let i = number; i >= 0; i--) {
	if(i <= 50) {
		console.log("The current value is " + i + ". Terminating the loop.")
		break;
	}; 
	if(i % 10 === 0) {
		console.log("The number is divisible by 10. Skipping the number.")
		continue
	};
	if(i % 5 === 0) {
		console.log(i);
	};
}

let name = "supercalifragilisticexpialidocious";
let consonant = "";

for(let i = 0; i < name.length; i++){
	if (
		name[i].toLowerCase() == "a" ||
		name[i].toLowerCase() == "e" ||
		name[i].toLowerCase() == "i" ||
		name[i].toLowerCase() == "o" ||
		name[i].toLowerCase() == "u" 
		) {
		continue
	}else {
		consonant += name[i];
	}
}
console.log(name);
console.log(consonant);